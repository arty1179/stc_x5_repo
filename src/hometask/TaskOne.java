package hometask;

import java.util.Scanner;

public class TaskOne {

    public static final String ERROR = "Error";

    public static void main(String[] args) {
        int previous = 0;
        int numberTarget = 1 + (int) (Math.random() * 100);

        //System.out.println(numberTarget);
        Scanner console = new Scanner(System.in);
        do {
            if (console.hasNextInt()) {
                int current = console.nextInt();
                int diffCurent = Math.abs(current - numberTarget);
                int diffPrevious = Math.abs(previous - numberTarget);
                validate(current);
                if (current == numberTarget) {
                    System.out.println("Success");
                    break;
                } else if (diffPrevious < diffCurent) {
                    System.out.println("Cool");
                } else if (diffPrevious > diffCurent) {
                    System.out.println("Hot");
                }
                previous = current;
            } else if (console.hasNext("стоп")) {
                break;
            } else {
                System.out.println(ERROR);
            }
        }
        while (true);
        console.close();
    }

    private static void validate(int current) {
        if (current < 1 || current > 100) {
            System.out.println(ERROR);
        }
    }
}
