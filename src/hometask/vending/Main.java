package hometask.vending;


import hometask.vending.drinks.ColdDrink;
import hometask.vending.drinks.Drink;
import hometask.vending.drinks.HotDrink;

public class Main {

    public static void main(String[] args) {

        Drink[] hotDrinks = new HotDrink[]{new HotDrink("Чай", 150)};
        Drink[] coldDrinks = new ColdDrink[]{new ColdDrink("Кола", 50)};

        VendingMachine vm = new VendingMachine(hotDrinks);

        vm.addMoney(200);
        vm.giveMeADrink(0);

        vm.setDrinks(coldDrinks);
        vm.giveMeADrink(0);
    }
}