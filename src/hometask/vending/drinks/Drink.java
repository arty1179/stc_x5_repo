package hometask.vending.drinks;

public interface Drink {
    double getPrice();
    String getTitle();
}