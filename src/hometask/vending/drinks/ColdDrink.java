package hometask.vending.drinks;

public class ColdDrink implements Drink {
    private String title;
    private double price;

    public ColdDrink(String title, double price) {
        this.title = title;
        this.price = price;
    }

    @Override
    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String getTitle() {
        return title;
    }
}