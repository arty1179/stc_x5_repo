package hometask.vending;


import hometask.vending.drinks.Drink;

public class VendingMachine {
    private double money = 0;
    private Drink[] drinks;

    public VendingMachine() {
    }

    public VendingMachine(Drink[] drinks) {
        this.drinks = drinks;
    }

    public void addMoney(double money) {
        this.money = money;
    }

    private Drink getDrink(int key) {
        if (key < drinks.length){
            return drinks[key];
        } else {
            return null;
        }
    }

    public void giveMeADrink(int key) {
        if (this.money > 0) {
            Drink drink = getDrink(key);

            if (drink != null) {
                if (drink.getPrice() <= money) {
                    System.out.println("Возьмите ваш напиток: " + drink.getTitle());
                    money -= drink.getPrice();
                } else {
                    System.out.println("Недостаточно средств!");
                }
            }
        } else {
            System.out.println("Бесплатно не работаем!");
        }
    }

    public void setDrinks(Drink[] drinks) {
        this.drinks = drinks;
    }
}
