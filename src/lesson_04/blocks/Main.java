package lesson_04.blocks;

public class Main {

    public static void main(String[] args) {
        long sum = 0;
      long start = System.currentTimeMillis();
        for (int i = 0; i < 1_000_000_000; i++) {
            sum += i;
            if (i % 1000 == 0) {
                System.out.println(i);
            }
        }
      long finish = System.currentTimeMillis();
      System.out.print("Worked ");
      System.out.println((finish - start) / 1000 + " sec");
    }
}
