package lesson_05.cast;

/**
 * Created by arty on 01.12.2018.
 */
public class Point implements Cloneable{
    public int x, y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public boolean equals(Object o) {
        if (o instanceof Point) {
            Point p = (Point) o;
            return p.x == x && p.y == y;
        }
        return false;
    }

    public static void main(String[] args) {
        Point point = new Point(10, 20);
        Point pointClone = new Point(10, 20);
        Point point2 = new Point(20, 10);

        System.out.println(point.equals(point2));
        System.out.println(point.equals(pointClone));

    }

    public Object clone() {
        Point p = null;
        try {
            p = (Point)super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return p;
    }
}