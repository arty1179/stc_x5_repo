package lesson_05.cast;

/**
 * Created by arty on 18.12.2018.
 */
public class Test implements Cloneable {
    Point p;
    int height;

    public Test(int x, int y, int z) {
        p = new Point(x, y);
        height = z;
    }

    public static void main(String s[]) {
        Test t1 = new Test(1, 2, 3), t2 = null;
        try {
            t2 = (Test) t1.clone();
        } catch (CloneNotSupportedException e) {
        }
        t1.p.x = -1;
        t1.height = -1;
        System.out.println("t2.p.x=" + t2.p.x + ", t2.height=" + t2.height);
    }

/*    public Object clone() throws CloneNotSupportedException {
        Test clone = null;
        clone = (Test) super.clone();
        clone.p = (Point) this.p.clone();
        return clone;
    }*/
}
