package lesson_05.generic.generic;

public class GenericBox <T1> {
    private T1 value;

    public GenericBox(T1 value) {
        this.value = value;
    }

    public T1 getValue() {
        return value;
    }

    public void setValue(T1 value) {
        this.value = value;
    }
}
