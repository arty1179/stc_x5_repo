package lesson_07.utils.tetris;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Lab3Win implements KeyListener {

    private JFrame frmLab;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    Lab3Win window = new Lab3Win();
                    window.frmLab.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public Lab3Win() {
        tetrisArray = new TetrisArray();
        initialize();
    }

    TetrisArray tetrisArray;
    private JPanel panel;
    private JLabel lblGameResult;
    private JLabel lblGameOver;
    private Timer timer;

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frmLab = new JFrame();
        frmLab.setTitle("Lab3 - tetris array prototype (with keyboard control)");
        frmLab.setBounds(100, 100, 450, 300);
        frmLab.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frmLab.getContentPane().setLayout(null);
        frmLab.addKeyListener(this);

        panel = new TetrisPanel(tetrisArray);
        panel.setBounds(10, 11, 249, 180);
        frmLab.getContentPane().add(panel);

        lblGameResult = new JLabel("Count = 0");
        lblGameResult.setBounds(284, 35, 113, 14);
        frmLab.getContentPane().add(lblGameResult);

        lblGameOver = new JLabel("");
        lblGameOver.setFont(new Font("Traditional Arabic", Font.BOLD, 14));
        lblGameOver.setBounds(277, 84, 131, 14);
        frmLab.getContentPane().add(lblGameOver);

        // Timer will be called every 500 msec
        timer = new Timer(500, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (tetrisArray.figureExist()) {
                    tetrisArray.fallOneLine();
                    panel.repaint();
                } else {
                    tetrisArray.addFigure();
                    panel.repaint();
                }
                printGameResult();
            }
        });
        timer.start();
    }

    private void printGameResult() {
        String res = "Count = " + tetrisArray.getGameResult();
        lblGameResult.setText(res);
        if (tetrisArray.isGameOver()) {
            lblGameOver.setText("GAME OVER!");
        }
    }

    @Override
    public void keyReleased(KeyEvent arg0) {
        switch (arg0.getKeyCode()) {
        case KeyEvent.VK_LEFT:
            tetrisArray.toLeft();
            panel.repaint();
            break;
        case KeyEvent.VK_RIGHT:
            tetrisArray.toRight();
            panel.repaint();
            break;
        case KeyEvent.VK_DOWN:
            tetrisArray.fallToBottom();
            panel.repaint();
            break;
        default:
            return;
        }
        printGameResult();
    }

    @Override
    public void keyPressed(KeyEvent arg0) {
        // TODO Auto-generated method stub
    }

    @Override
    public void keyTyped(KeyEvent e) {
        // TODO Auto-generated method stub
    }
}
