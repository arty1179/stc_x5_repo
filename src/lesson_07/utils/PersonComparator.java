package lesson_07.utils;

import java.util.Comparator;

/**
 * Created by arty on 12.09.2018.
 */
public class PersonComparator implements Comparator<PersonSimple> {

    public int compare(PersonSimple a, PersonSimple b) {

        return b.getName().compareTo(a.getName());
    }

}