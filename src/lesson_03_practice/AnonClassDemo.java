package lesson_03_practice;

/**
 * Created by a.panasyuk on 29.06.2018.
 */
interface Message {
    String greet();
}
interface Contents {
}

public class AnonClassDemo {
    public void displayMessage(Message m) {
        System.out.println(m.greet() +
                ", это пример анонимного внутреннего класса в качестве аргумента");
    }
    public Contents contents() {
        return new Contents(){
            private int i = 11;
        };
    }

    public Contents contents2() {
        return new MyContents(){
        };
    }

    private class MyContents implements Contents {
        private int i = 11;
    }

    public static void main(String args[]) {
        // Создание класса
        AnonClassDemo obj = new AnonClassDemo();
        Contents c = obj.contents();
        // Передача анонимного внутреннего класса в качестве аргумента
        obj.displayMessage(new Message() {
            public String greet() {
                return "Привет";
            }
        });

    }


}

