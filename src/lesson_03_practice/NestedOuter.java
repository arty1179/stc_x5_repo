package lesson_03_practice;

/**
 * Created by a.panasyuk on 29.06.2018.
 */
public class NestedOuter {
    private static int i = 1;
    private int ii = 1;

    static class NestedInner {
        void print() {
            System.out.println("i'm static nested!!!");
            System.out.println(i);
        }

        static void staticPrint() {
            System.out.println("i'm static nested!!!");
            NestedOuter no = new NestedOuter();
            System.out.println(no.ii);
        }
    }
}

class Demo {
    public static void main(String[] args) {
        NestedOuter.NestedInner nd = new NestedOuter.NestedInner();
        nd.print();
        NestedOuter.NestedInner.staticPrint();
    }
}

