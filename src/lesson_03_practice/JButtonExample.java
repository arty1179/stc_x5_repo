package lesson_03_practice;

/**
 * Created by a.panasyuk on 29.06.2018.
 */
import java.awt.event.*;
import javax.swing.*;
public class JButtonExample {
    public static void main(String[] args) {
        JFrame f=new JFrame("Button Example");
        final JTextField tf=new JTextField();
        tf.setBounds(50,50, 250,20);
        JButton b=new JButton("Input  name and click!");
        b.setBounds(50,100,250,30);
        b.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                tf.setText("Welcome to Java, " + tf.getText() + "!");
            }
        });
        f.add(b);f.add(tf);
        f.setSize(400,400);
        f.setLayout(null);
        f.setVisible(true);
    }
}