package lesson_03_practice.first;

/**
 * Created by arty on 01.12.2018.
 */
public class Provider {
    public Parent getValue() {
        return new Child();
    }
}