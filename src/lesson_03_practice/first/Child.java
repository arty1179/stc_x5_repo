package lesson_03_practice.first;

/**
 * Created by arty on 01.12.2018.
 */
// Класс Child наследуется от класса Parent,
// но имеет ограничение доступа по умолчанию
class Child extends Parent {
}
